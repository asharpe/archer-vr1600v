#!/bin/bash

function log() {
	(
		(($#)) && echo "$@" || cat
	) >&2
}

function length() {
	wc -c | tr -d '[[:space:]]'
}

[[ -d "$1" ]] || {
	log "First argument needs to be a directory containing JSON files"
	exit 1
}

log <<-EOF

	got connection
EOF

headers=()
# timeout in case things are taking too long
while IFS=$'\r\n' read -t 10 line
do
	headers+=("$line")

	# no more headers
	[[ -z $line ]] && break

	echo "${line}" >&2
done

(($?)) && {
	printf "HTTP/1.0 500 Internal Server Error\r\n"
	error=$'Timeout reading headers\r\n'
	length=$(printf "$error" | length)
	printf "Content-Length: $length\r\n"
	printf "\r\n"
	printf "$error"
	exit 1
}

log "loading data"
output=$(for f in "$1"/*.json
do
	d=$(basename ${f%.json})
	jq --arg d $d '{date:($d|tonumber),data:{download,upload,server}}' $f
done | jq -s .)$'\r\n'

length=$(printf "$output" | length)

log "responding"
printf "HTTP/1.0 200 OK\r\n"
printf "Access-Control-Allow-Origin: *\r\n"
printf "Content-Type: application/json\r\n"
printf "Content-Length: $length\r\n"
printf "\r\n"
printf "$output"

