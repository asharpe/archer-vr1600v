# archer-VR1600v

Hacks related to the NBN Archer VR1600R

## Links
Useful links to help you get the most out of your modem:

 * https://www.marcelvarallo.com/some-more-fiddling-with-the-archer-vr1600v/
 * https://setuprouter.com/router/tp-link/archer-vr1600v/manual-2515.pdf

## Uptime information
This is gathered by logging into the modem via telnet and querying the `ewan_pppoe` interface.  Eg.

```
expect -f archer-VR1600v-interface-info.exp <username> <password> <interface>
```

## Speed test logging
Install `speedtest-cli` via homebrew and setup a logging directory (writable by your user).

```
brew install speedtest-cli
sudo mkdir -p /var/log/speedtest
sudo chown $(id -un) /var/log/speedtest
```

Then throw this bad boy in a crontab which looks a bit like this on OSX

```
10,30,50 * * * * /usr/local/bin/speedtest --json >/var/log/speedtest/$(date +\%s).json
```

## Viewing speed test data
This is a horrendous hack just to get the captured data available via a web interface.  You'll need `socat` installed for this...

```
socat TCP4-LISTEN:8080,reuseaddr,fork EXEC:"./speedtest-data-server.bash /var/log/speedtest"
```

## Rendering the uptime information
Assuming the data is captured as above, and the server is running as above, simply open `./speedtest.html` in your browser (give it a couple of seconds to massage the data and render it)

